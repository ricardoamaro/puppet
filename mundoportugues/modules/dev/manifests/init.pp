class dev {

file { '/home/dev/.ssh/authorized_keys':
  ensure  => 'file',
  mode    => '0700',
  owner   => 'dev',
  group   => 'dev',
  source  => "puppet:///modules/dev/authorized_keys";
}

file { '/home/teste/.ssh/authorized_keys':
  ensure  => 'file',
  mode    => '0700',
  owner   => 'teste',
  group   => 'teste',
  source  => "puppet:///modules/os/authorized_keys";
}

}
