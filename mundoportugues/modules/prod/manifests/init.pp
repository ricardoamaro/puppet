class prod {

file { '/home/grupomundoportugues/.ssh/authorized_keys':
  ensure  => 'file',
  mode    => '0700',
  owner   => 'grupomundoportugues',
  group   => 'grupomundoportugues.pt',
  source  => "puppet:///modules/prod/authorized_keys";
}

}
