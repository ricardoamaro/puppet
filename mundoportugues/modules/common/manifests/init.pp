class common {

#file {
#  "/etc/apt/sources.list.d/webmin.list":
#  ensure => file,
#  source =>  "puppet:///modules/common/webmin.list",
#}


# a fuller example, including permissions and ownership
file { '/etc/apache2/suexec':
	ensure => 'directory',
	owner  => 'root',
	group  => 'root',
	mode   => '0755',
}

file {
  "/etc/apache2/suexec/www-data":
  ensure => file,
  source =>  "puppet:///modules/common/suexecwwwdata",
  require => File['/etc/apache2/suexec'],
}

# install packages
  $enhancers = [ 'screen', 'strace', 'sudo', 'htop', 'mc', 'byobu', 'git', 'vim', 'tig', 'ruby', 'wget', 'curl', 'diffutils', 'drush']
  package { $enhancers: ensure => 'installed'  }

}
