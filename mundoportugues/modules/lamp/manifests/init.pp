class lamp {
#execute 'apt-get update'
exec { 'apt-update':                    # exec resource named 'apt-update'
  command =>  '/usr/bin/apt-get update'  # command this resource will run
}

# install apache2 package
package { 'apache2':
  ensure  =>  installed,
}

# install libapache2-mod-php5 package
package { 'libapache2-mod-php5':
  require => Package['apache2'],
  ensure  =>  installed,
}

# ensure apache2 service is running
service { 'apache2':
  ensure =>  running,
  require => Package['apache2'],
}

# install mysql-server package
package { 'mysql-server':
  ensure  =>  installed,
}

# ensure mysql service is running
service { 'mysql':
  ensure =>  running,
  require => Package['mysql-server'],
}

exec { 'mysql_pass':
  command   => '/bin/cat /etc/mysql/debian.cnf > /root/.my.cnf',
  cwd       => '/root/',
  unless    => '/usr/bin/diff -q /etc/mysql/debian.cnf /root/.my.cnf',
}

# install packages for lamp
  $enhancers = [ 'php5','php5-cli','php5-curl', 'php5-gd', 'php5-imap', 'php5-memcache', 'php5-memcached', 'php5-mysql', 'php5-mcrypt', 'php-pear']
  package { $enhancers: ensure => 'installed',  require => Package['libapache2-mod-php5'], }

# Several configs bellow for apache php
exec { 'opcache_active':
  command   => '/bin/sed -i "s/^.*opcache.enable=.*/opcache.enable=1/" php.ini',
  cwd       => '/etc/php5/apache2/',
  unless    => '/bin/grep -Fxq "opcache.enable=1" php.ini',
  require => Package['libapache2-mod-php5'],
}

exec { 'opcache_memory':
  command   => '/bin/sed -i "s/^.*opcache.memory_consumption=.*/opcache.memory_consumption=128/" php.ini',
  cwd       => '/etc/php5/apache2/',
  unless    => '/bin/grep -Fxq "opcache.memory_consumption=128" php.ini',
  require => Package['libapache2-mod-php5'],
}

exec { 'php_memory':
  command   => '/bin/sed -i "s/^.*memory_limit.*/memory_limit=192M/" php.ini',
  cwd       => '/etc/php5/apache2/',
  unless    => '/bin/grep -Fxq "memory_limit=192M" php.ini',
  require => Package['libapache2-mod-php5'],
}

exec { 'php_timezone':
  command   => '/bin/sed -i "s|^.*date.timezone =.*|date.timezone = \"Europe/Lisbon\"|" php.ini',
  cwd       => '/etc/php5/apache2/',
  unless    => '/bin/grep -Fxq "date.timezone = \"Europe/Lisbon\"" php.ini',
  require => Package['libapache2-mod-php5'],
}

# Several configs bellow for cli php
exec { 'opcache_active_cli':
  command   => '/bin/sed -i "s/^.*opcache.enable=.*/opcache.enable=1/" php.ini',
  cwd       => '/etc/php5/cli/',
  unless    => '/bin/grep -Fxq "opcache.enable=1" php.ini',
}

exec { 'php_timezone_cli':
  command   => '/bin/sed -i "s|^.*date.timezone =.*|date.timezone = \"Europe/Lisbon\"|" php.ini',
  cwd       => '/etc/php5/cli/',
  unless    => '/bin/grep -Fxq "date.timezone = \"Europe/Lisbon\"" php.ini',
}

# install apache2-suexec-custom package
package { 'apache2-suexec-custom':
  require => Package['apache2'],
  ensure  =>  installed,
}

# install memcached package
package { 'memcached':
  ensure  =>  installed,
}

exec { 'memcached_memory':
  command   => "/bin/sed -i 's/-m 64/-m 256/g' memcached.conf",
  cwd       => '/etc/',
  unless    => '/bin/grep -Fxq "-m 256" memcached.conf',
}

# ensure memcache service is running
service { 'memcached':
  ensure =>  running,
}

file { '/etc/php5/mods-available/memcache.ini':
  ensure  => 'file',
  source  => "puppet:///modules/lamp/memcache.ini";
}

# install postfix package
package { 'postfix':
  ensure  =>  installed,
}

# ensure postfix service is running
service { 'postfix':
  ensure =>  running,
}

# ensure info.php file exists
#file { '/var/www/html/info.php':
#  ensure  => file,
#  content => '<?php  phpinfo(); ?>',    # phpinfo code
#  require =>  Package['apache2'],        # require 'apache2' package before creating
#}

}
