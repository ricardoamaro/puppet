class backup {

# install rsnapshot package
package { 'rsnapshot':
	require => Exec['apt-update'],
	ensure  =>  installed,
}

# ensure automysqlbackup package
package { 'automysqlbackup':
	require => Exec['apt-update'],
	ensure =>  installed,
}

# a fuller example, including permissions and ownership
file { '/backup':
	ensure => 'directory',
	owner  => 'root',
	group  => 'root',
	mode   => '0700',
}

file { "/etc/rsnapshot.conf":
	ensure => file,
	source => "puppet:///modules/backup/rsnapshot.conf";
}

file { "/etc/cron.d/rsnapshot":
	ensure => file,
	source => "puppet:///modules/backup/rsnapshot.cron";
}

file { "/etc/default/automysqlbackup":
	ensure => file,
	source => "puppet:///modules/backup/automysqlbackup";
}

file { "/etc/cron.d/automysqlbackup":
	ensure => file,
	source => "puppet:///modules/backup/automysqlbackup.cron";
}

}

