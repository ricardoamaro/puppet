class os {
file { '/etc/motd':
  ensure => 'file',
  source => 'puppet:///modules/os/motd';
}

file { '/root/.forward':
  ensure  => file,
  content => 'webmaster@cd-ai.com',
}

file { '/root/.ssh/authorized_keys':
  ensure  => 'file',
  mode    => '0700',
  owner   => 'root',
  group   => 'root',
  source  => "puppet:///modules/os/authorized_keys";
}

file { '/etc':
  ensure  => 'directory',
  mode    => '0711',
}

file { '/etc/webmin':
  ensure  => 'directory',
  mode    => '0711',
}

file { '/home/cdai':
  ensure  => 'directory',
  mode    => '0700',
}

}
