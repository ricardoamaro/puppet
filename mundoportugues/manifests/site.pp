node default {
  include os
}

node 'dev.mundoportugues.local' {
  include os, common, lamp, backup, dev
}

node 'prod.mundoportugues.local' {
  include os, common, lamp, backup, prod
}
